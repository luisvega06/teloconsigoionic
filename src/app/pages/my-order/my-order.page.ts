import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { AlertController } from '@ionic/angular';
import { AuthserviceService } from '../../authservice.service';
import { JwtService } from '../../jwt.service';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.page.html',
  styleUrls: ['./my-order.page.scss'],
})
export class MyOrderPage implements OnInit {
  DELETE_RESERVATION = this.authService.baseURL + "reservation/";
  CONFIRMAR_PEDIDO = this.authService.baseURL + "reservation/order";
  myReservations = [];


  infoGlobal = [];

  info = [
    {
      discount_vouncher_id: "1",
      payment_method_id: "",
      domiciliary_id: "1",
      total: 0
    }
  ]
  myOrders = {};
  minimo = 9999;
  count: number = 0;
  user: any = {};
  totalValor = 0;
  totalproduct = 0;
  quantityProduct;
  valueCupon = '';
  validCupon = 0;
  noValidCupon = 0;
  number;
  cupones: any = [

  ];
  metodoPagos: any = [

  ];

  constructor(public alertController: AlertController,
    private authService: AuthserviceService,
    private jwtService: JwtService,
    public navCtrl: NavController,
    private router: Router) { }

  async getTotal() {
    this.totalValor = 0;
    this.totalproduct = 0;
    for (let total of this.myReservations) {
      this.totalValor = this.totalValor + total.reservationValueTotal;
      this.totalproduct = this.totalproduct + total.reservationQuantity;
      //console.log(this.totalproduct);
    }

    this.number = new Intl.NumberFormat("de-DE").format(this.totalValor);

  }

  async getCupones() {
    const token = localStorage.getItem('TokenTeloConsigo');
    const data = this.authService.get(this.authService.baseURL + "discount_vounchers", token)
      .then(data => {
        this.cupones = data.data.discountVouncher;
        //console.log(this.cupones);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });

    //console.log(this.cupones);
  }

  getToken() {
    try {
      const token = localStorage.getItem('TokenTeloConsigo');
      console.log(token);
      // console.log(this.parseJwt(token));
      this.user = this.jwtService.parseJwt(token);
      //console.log(this.user);
      if (token == '') {
        this.navCtrl.navigateForward(['login']);
      } else {
        this.getMyOrder();
      }
    } catch (ex) {
      console.log(ex);
    }
  }

  async numberFormat(total) {
      new Intl.NumberFormat("de-DE").format(this.totalValor);
  }

  async getMetodoPago() {
    const token = localStorage.getItem('TokenTeloConsigo');
    const data = this.authService.get(this.authService.baseURL + "payment_methods", token)
      .then(data => {
        this.metodoPagos = data.data.paymentMethod;
        //console.log(this.metodoPagos);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });


  }


  buscarCupon(event) {
    this.valueCupon = event.detail.value;
    //console.log(this.valueCupon);
    for (let cupon of this.cupones) {
      if (cupon.code == this.valueCupon && cupon.state == 1) {
        console.log("cupon valido");
        this.validCupon = 1;
        this.noValidCupon = 0;
        this.totalValor = this.totalValor - cupon.price;
        this.info[0].discount_vouncher_id = cupon.id;
        break;
      } else {
        if (cupon.code != this.valueCupon && this.valueCupon != "") {
          console.log("cupon invalido");
          this.noValidCupon = 1;
          this.totalValor = 0;
          this.validCupon = 0;
          this.totalproduct = 0;
          this.getTotal();
        }else{
          if (this.valueCupon == "") {
            this.validCupon = 0;
            this.noValidCupon = 0;
            this.totalValor = 0;
            this.totalproduct = 0;
            this.getTotal();
          }
        }
        
      }
    }
  }

  async presentAlertConfirm(reservation_id) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmación!',
      message: ' <strong>Esta seguro de eliminar esta orden</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Eliminar',
          handler: () => {
            console.log('Confirm Okay');
            this.deleteReservation(reservation_id);
          }
        }
      ]
    });

    await alert.present();
  }


  async deleteReservation(reservation_id) {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(reservation_id);
    //this.presentAlertConfirm();

    const data = this.authService.get(this.DELETE_RESERVATION + reservation_id, token)
    .then(data => {
      //console.log(data.data.msj);
      this.getMyOrder();
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
      //this.presentAlert(error.response.data.data)
    });
    
  }

  ngOnInit() {
    this.getToken();
    this.getCupones();
    this.getMetodoPago();

  }

  ionViewWillEnter(){
    this.getToken();
    this.getCupones();
    this.getMetodoPago();
    //this.numberFormat();
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Te lo consigo dice:',
      subHeader: 'Información..!',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }


  async enviar() {
    const token = localStorage.getItem('TokenTeloConsigo');
    this.info[0].total = this.totalValor;
    this.infoGlobal.push(this.myReservations);
    this.infoGlobal.push(this.info);

    console.log(this.infoGlobal);
    // console.log(this.info);
    this.authService.post(this.CONFIRMAR_PEDIDO, this.infoGlobal, token)
      .then(data => {
        //localStorage.setItem("TokenTigoView",data.data.token);
        this.presentAlert("Pedido enviado en poco tiempo sera contactado.!");
        this.router.navigate(['/my-reserve']);
        console.log(data);
      })
      .catch(error => {
        console.log(this.myReservations);

      });
  }

  async validator() {
    if (this.info[0].payment_method_id == '') {
      this.presentAlert("Elija un metodo de pago");
    } else {
      if (this.noValidCupon == 1) {
        this.presentAlert("El cupon es invalido");
      } else {
        this.enviar();
      }

    }
  }


  async orderMinimum() {
    this.presentAlert("la orden minima es de 10.000 pesos por favor añada otro producto o agregue otra cantidad");
    console.log("pedido minimo");
  }


  async getMyOrder() {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(this.user);
    const data = this.authService.get(this.authService.baseURL + "reservation/my-reservations/" + this.user.id, token)
      .then(data => {
        this.myReservations = data.data.reservations;
        console.log(data);
        this.getTotal();
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
    //this.myReservations = data.data.reservations;
    //console.log(this.myReservations);

  }

  async updateReservationQuantity(reserva_id, quantity) {
    const token = localStorage.getItem('TokenTeloConsigo');
    const data = this.authService.post(this.authService.baseURL + "reservation/update-quantity/" + reserva_id + "/" + quantity, this.info, token)
      .then(data => {
        //this.myReservations = data.data.reservation;
        console.log(this.myReservations);
        console.log("enviada la suma");
        this.getMyOrder();
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
      });

  }


  refresh(): void {
    window.location.reload();
  }



  addRemove(symbol: string = '+', reservationId, quantityProduct, quantityReserva) {
    console.log('Cuantity')  
    console.log(symbol, reservationId, quantityProduct, quantityReserva);
    if (symbol == '+') {

      if (quantityReserva == quantityProduct) {
        this.presentAlert("En el momento solo tenemos " + quantityReserva + " de este producto en inventario");
      } else {
        console.log(quantityProduct, quantityReserva);
        quantityReserva++;
        this.updateReservationQuantity(reservationId, quantityReserva);
        //this.refresh();
      }

    } else {
      if (quantityReserva - 1 > 0) {

        quantityReserva--;
        this.updateReservationQuantity(reservationId, quantityReserva);
        //this.refresh();
      } else {
        this.presentAlert("La cantidad minima es 1, Gracias..!!");
      }
    }
  }

}
