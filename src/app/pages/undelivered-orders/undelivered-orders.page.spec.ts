import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UndeliveredOrdersPage } from './undelivered-orders.page';

describe('UndeliveredOrdersPage', () => {
  let component: UndeliveredOrdersPage;
  let fixture: ComponentFixture<UndeliveredOrdersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UndeliveredOrdersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UndeliveredOrdersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
