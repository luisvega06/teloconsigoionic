import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UndeliveredOrdersPage } from './undelivered-orders.page';

const routes: Routes = [
  {
    path: '',
    component: UndeliveredOrdersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UndeliveredOrdersPageRoutingModule {}
