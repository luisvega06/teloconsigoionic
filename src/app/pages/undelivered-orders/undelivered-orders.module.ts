import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UndeliveredOrdersPageRoutingModule } from './undelivered-orders-routing.module';

import { UndeliveredOrdersPage } from './undelivered-orders.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UndeliveredOrdersPageRoutingModule
  ],
  declarations: [UndeliveredOrdersPage]
})
export class UndeliveredOrdersPageModule {}
