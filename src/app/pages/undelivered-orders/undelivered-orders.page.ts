import { Component, OnInit } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';

@Component({
  selector: 'app-undelivered-orders',
  templateUrl: './undelivered-orders.page.html',
  styleUrls: ['./undelivered-orders.page.scss'],
})
export class UndeliveredOrdersPage implements OnInit {

  stateOrder = 3;
  orderNo = [];
  user: any = {};

  constructor( public navCtrl: NavController,
               private  authService :  AuthserviceService,
               private jwtService: JwtService ) { }

  ngOnInit() {
    this.getToken();
  }

  ionViewWillEnter() {
    this.getToken();
	}

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.getOrder();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  async getOrder() {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null){

    }else{
      const data = this.authService.get(this.authService.baseURL + "orders/get-order-by-state/"+this.stateOrder, token)
      .then(data =>{
        this.orderNo = data.data.orders;
        console.log(this.orderNo);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
    }
  }

  async orderDetail(id) { 
		console.log(id);
		let navigationExtras: NavigationExtras = {
		  queryParams: {
			orderId: JSON.stringify(id)
		  }
		};
		this.navCtrl.navigateForward(['/order-undelivered'], navigationExtras);
	}

}
