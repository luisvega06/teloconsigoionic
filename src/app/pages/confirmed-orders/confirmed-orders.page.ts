import { Component, OnInit } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';

@Component({
  selector: 'app-confirmed-orders',
  templateUrl: './confirmed-orders.page.html',
  styleUrls: ['./confirmed-orders.page.scss'],
})
export class ConfirmedOrdersPage implements OnInit {

  ordersPendings = [];
	pendiente = 0;
	count:number=0;
	stateSend = 1;
	user: any = {};

  constructor( public navCtrl: NavController,
			   private  authService :  AuthserviceService,
			   private jwtService: JwtService ) { }

  ngOnInit() {
    this.getToken();
  }
  ionViewWillEnter() {
    this.getToken();
  }

  async getOrdespending() {

		const token = localStorage.getItem('TokenTeloConsigo');	
		const data = this.authService.get(this.authService.baseURL + "orders/get-order-by-state/"+this.stateSend, token)
		.then(data =>{
			this.ordersPendings = data.data.orders;
			console.log(this.ordersPendings);
			})
		.catch(error => {
		console.log(error.response);
		this.navCtrl.navigateForward(['login']);
		//this.presentAlert(error.response.data.data)
		});
		
	
	
  	}

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.getOrdespending();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }
  
  async manageOrder(id) {
		console.log(id);
		let navigationExtras: NavigationExtras = {
		  queryParams: {
			orderId: JSON.stringify(id)
		  }
		};
		this.navCtrl.navigateForward(['/confirm-order'], navigationExtras);
	}

}
