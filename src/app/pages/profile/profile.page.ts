import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../../authservice.service';
import { NavController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {


  URL_USER_UPDATE = this.authService.baseURL + "user/update/"
  user: any = {};
  credentials: any ={
    name: "",
    phone: "",
    direction: ""
  };
  

  constructor( private jwtService: JwtService,
               private  authService            :  AuthserviceService,
               public navCtrl: NavController,
               public alertController          :  AlertController ) { }


  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        //console.log(token);
        this.user = this.jwtService.parseJwt(token);
        this.credentials = this.user;
        if(this.user.rol == 2){
          this.navCtrl.navigateForward(['home']);
        }
          
        
      }
      
      //console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async updateInfo() {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(this.credentials);
    this.authService.post(this.URL_USER_UPDATE+this.user.id, this.credentials, token)
    .then(data =>{
      this.presentAlert("datos actualizado correctamente");
      //console.log(data);
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });
  }

  ngOnInit() {
    this.getToken();
  }
  ionViewWillEnter() {
    this.getToken();
	}


}
