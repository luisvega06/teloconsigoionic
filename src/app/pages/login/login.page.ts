import { Component, OnInit, AfterContentInit, AfterViewInit,OnDestroy } from '@angular/core';
import { Router } from  "@angular/router";
import { JwtService } from '../../jwt.service';
import { MenuController } from '@ionic/angular';
import { AuthserviceService } from '../../authservice.service';
import { AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import axios from 'axios';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentials = {
    email: "luisprofee@gmail.com",
    password: "secret"
  };
  credentialsCopy = {
    email: "",
    password: ""
  };
  
  user: any = {};
  LOGIN_URL = this.authService.baseURL +'auth/login';
  

  hideInputPassword:boolean=true;

  constructor(
    private  authService      :  AuthserviceService,
    private  router           :  Router, 
    public alertController    :  AlertController,
    public navCtrl            :  NavController,
    public menuCtrl: MenuController,
    private jwtService: JwtService,
    public loadingController: LoadingController) { }

    async viewPublications(){
      this.navCtrl.navigateForward(['tabs-client/home-client']);
    }

    async presentLoading() {
      const loading = await this.loadingController.create({
        cssClass: 'my-custom-class',
        message: 'Verificando información...',
        duration: 2000
      });
      await loading.present();
  
      const { role, data } = await loading.onDidDismiss();
      console.log('Loading dismissed!');
    }

    
    

    getToken() {
      try{
        const token = localStorage.getItem('TokenTeloConsigo');
  
        console.log(token);
        // console.log(this.parseJwt(token));
        this.user = this.jwtService.parseJwt(token);
        console.log(this.user);
        if(this.user.rol == 1){
          this.navCtrl.navigateForward(['home-client']);
        }else{
          if(this.user.rol == 2){
            this.navCtrl.navigateForward(['home']);
          }else{
            this.navCtrl.navigateForward(['pending-orders']);
          }
          
        }
      }catch( ex){
        console.log(ex);
      }
    }


  async login(){
    console.log(this.credentials);
    this.presentLoading();
    this.authService.post(this.authService.baseURL + 'auth/login', this.credentials, {})
    .then(data =>{
      localStorage.setItem("TokenTeloConsigo",data.data.token);
      this.getToken();

      //console.log(data);
      //this.viewPublications();
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });  
   }


   async presentAlert( message ) {
     const alert = await this.alertController.create({
       header: 'Error',
       subHeader: message,
       buttons: ['OK']
     });

     await alert.present();
   }

   async create(){
     this.navCtrl.navigateForward(['/register']);
   }
  

  ngOnInit() {
    this.credentials = this.credentialsCopy;
    
  }
  ionViewWillEnter() {
		this.credentials = this.credentialsCopy;
	}

}
