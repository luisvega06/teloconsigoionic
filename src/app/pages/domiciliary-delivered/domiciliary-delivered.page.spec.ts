import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DomiciliaryDeliveredPage } from './domiciliary-delivered.page';

describe('DomiciliaryDeliveredPage', () => {
  let component: DomiciliaryDeliveredPage;
  let fixture: ComponentFixture<DomiciliaryDeliveredPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomiciliaryDeliveredPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DomiciliaryDeliveredPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
