import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DomiciliaryDeliveredPageRoutingModule } from './domiciliary-delivered-routing.module';

import { DomiciliaryDeliveredPage } from './domiciliary-delivered.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DomiciliaryDeliveredPageRoutingModule
  ],
  declarations: [DomiciliaryDeliveredPage]
})
export class DomiciliaryDeliveredPageModule {}
