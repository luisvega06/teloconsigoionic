import { Component, OnInit } from '@angular/core';
import * as Chart from "chart.js";
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import { NavController } from '@ionic/angular';
import Axios from 'axios';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  user: any = {};

  myDashboard = [];
  dataSales = {};



  constructor( private jwtService: JwtService,
               private  authService :  AuthserviceService,
               public navCtrl            :  NavController ) { }

  async getDashboard() {
    const token = localStorage.getItem('TokenTeloConsigo');

    const data = this.authService.get(this.authService.baseURL + "order/dasboard", token)
    .then(data =>{
      this.myDashboard = data.data.orders;
    console.log(this.myDashboard);
    this.salesTotal();
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
      //this.presentAlert(error.response.data.data)
    });
    
  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.getDashboard();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  ngOnInit() {
    
    this.getToken();
  }

  ionViewWillEnter() {
    this.getToken();
  }

  salesTotal() {

    for(let a of this.myDashboard) {
      //console.log(a.sales[0].value);
      if(a.sales[0].value != null){
        //this.dataSales[a.sales[0].value] = a.sales[0].value;
        this.dataSales[a.date] = a.sales[0].value;
      }else{
        this.dataSales[a.date] = 0;
      }
    }

    console.log(this.dataSales);

    const ctx = document.getElementById('sales');
    const myChart = new Chart(ctx, {
      type: 'line',
      data: {
          labels: Object.keys(this.dataSales),
          datasets: [{
              label: 'Reporte de ventas',
              data: Object.values(this.dataSales),
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
    });
  }

}
