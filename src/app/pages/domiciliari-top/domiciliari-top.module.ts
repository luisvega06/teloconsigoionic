import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DomiciliariTopPageRoutingModule } from './domiciliari-top-routing.module';

import { DomiciliariTopPage } from './domiciliari-top.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DomiciliariTopPageRoutingModule
  ],
  declarations: [DomiciliariTopPage]
})
export class DomiciliariTopPageModule {}
