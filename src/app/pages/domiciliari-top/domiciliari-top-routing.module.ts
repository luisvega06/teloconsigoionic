import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DomiciliariTopPage } from './domiciliari-top.page';

const routes: Routes = [
  {
    path: '',
    component: DomiciliariTopPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DomiciliariTopPageRoutingModule {}
