import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DomiciliariTopPage } from './domiciliari-top.page';

describe('DomiciliariTopPage', () => {
  let component: DomiciliariTopPage;
  let fixture: ComponentFixture<DomiciliariTopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomiciliariTopPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DomiciliariTopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
