import { Component, OnInit, AfterContentInit, AfterViewInit,OnDestroy } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { NavController } from '@ionic/angular';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';

@Component({
  selector: 'app-my-reserve',
  templateUrl: './my-reserve.page.html',
  styleUrls: ['./my-reserve.page.scss'],
})
export class MyReservePage implements OnInit {
  myReservationsOk = [];
  totalArticulos = 0;
  totalPagar = 0;
  //state = 0;
  user: any = {};

  constructor( public menuCtrl: MenuController,
              private jwtService: JwtService,
              private  authService :  AuthserviceService,
              public navCtrl            :  NavController ) { }


  ngOnInit() {
    this.getToken();
    
  }

  formatNumber( valor : number = 0 ){
		return new Intl.NumberFormat("de-DE").format( valor );
	}

  ionViewWillEnter() {
    this.getToken();
	}
  /*async getTotal() {
    for(let total of this.myReservationsOk) {
      this.totalArticulos=this.totalArticulos+total.reservationQuantity;
      console.log(this.totalArticulos);
    }
    this.totalPagar = this.myReservationsOk[0]['orderTotal'];
    this.state = this.myReservationsOk[0]['orderState'];
    console.log(this.totalPagar);

  }*/

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');

      console.log(token);
      // console.log(this.parseJwt(token));
      this.user = this.jwtService.parseJwt(token);
      console.log(this.user);
      if(token == ''){
        this.navCtrl.navigateForward(['login']);
      }else{
        this.getMyOrder();
      }
    }catch( ex){
      console.log(ex);
    }
  }

  async getMyOrder() {
    const token = localStorage.getItem('TokenTeloConsigo');
    const data = this.authService.get(this.authService.baseURL + "orders/my-orders/"+this.user.id, token)
    .then(data =>{
      this.myReservationsOk = data.data.orders;
      console.log(this.myReservationsOk);
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
      //this.presentAlert(error.response.data.data)
    });
    
	  
    //this.getTotal();
  }

}
