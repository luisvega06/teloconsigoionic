import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { Router } from '@angular/router';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ToastController, ActionSheetController, ModalController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.page.html',
  styleUrls: ['./edit-product.page.scss'],
})
export class EditProductPage implements OnInit {

  device        : number  = 0;
  UPDATE_PRODUCT_URL = this.authService.baseURL + "product-update/"
  product = {
    name: "",
    picture: "",
    quantity: 0,
    price: 0,
    offer: 0,
    title: "",
    description: ""
  };
  product_id;
  credentials  : any = {
    name: "",
    quantity: "",
    price: "",
    title: "",
    description: "",
    category_id: 0,
    picture: '',
    state: 1,
    offer: ""
  };
  categories = [];
  user: any = {};

  public slideOpts = {
    on: {
      beforeInit() {
        const swiper = this;
        swiper.classNames.push(`${swiper.params.containerModifierClass}fade`);
        const overwriteParams = {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerGroup: 1,
          watchSlidesProgress: true,
          spaceBetween: 0,
          virtualTranslate: true,
        };
        swiper.params = Object.assign(swiper.params, overwriteParams);
        swiper.params = Object.assign(swiper.originalParams, overwriteParams);
      },
      setTranslate() {
        const swiper = this;
        const { slides } = swiper;
        for (let i = 0; i < slides.length; i += 1) {
          const $slideEl = swiper.slides.eq(i);
          const offset$$1 = $slideEl[0].swiperSlideOffset;
          let tx = -offset$$1;
          if (!swiper.params.virtualTranslate) tx -= swiper.translate;
          let ty = 0;
          if (!swiper.isHorizontal()) {
            ty = tx;
            tx = 0;
          }
          const slideOpacity = swiper.params.fadeEffect.crossFade
            ? Math.max(1 - Math.abs($slideEl[0].progress), 0)
            : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
          $slideEl
            .css({
              opacity: slideOpacity,
            })
            .transform(`translate3d(${tx}px, ${ty}px, 0px)`);
        }
      },
      setTransition(duration) {
        const swiper = this;
        const { slides, $wrapperEl } = swiper;
        slides.transition(duration);
        if (swiper.params.virtualTranslate && duration !== 0) {
          let eventTriggered = false;
          slides.transitionEnd(() => {
            if (eventTriggered) return;
            if (!swiper || swiper.destroyed) return;
            eventTriggered = true;
            swiper.animating = false;
            const triggerEvents = ['webkitTransitionEnd', 'transitionend'];
            for (let i = 0; i < triggerEvents.length; i += 1) {
              $wrapperEl.trigger(triggerEvents[i]);
            }
          });
        }
      },
    }
  }

  constructor( private route: ActivatedRoute,
              public alertController          :  AlertController,
              private  authService            :  AuthserviceService,
              protected toastCtrl             : ToastController,
              protected camera                : Camera,
              protected platform              : Platform,
              protected actionSheetController : ActionSheetController,
              public navCtrl            :  NavController,
              private jwtService: JwtService,
              private router: Router ) { 

                if( this.platform.is("desktop") ){
                  this.device = 1;
                }else if( this.platform.is("android") ){
                  this.device = 2;
                }else if( this.platform.is("ios") ){
                  this.device = 3;
                }else{
                  this.device = 1;
                }
              }

    onFileChange(event){
      console.log('picture' , event.target.files);
      
    }

  async getValueProduct() {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null){
      this.navCtrl.navigateForward(['login']);
    }else{
      const data = this.authService.get(this.authService.baseURL + "products/"+this.product_id, token)
      .then(data =>{
        this.product = data.data.product;
        this.credentials.picture = this.product.picture;
        console.log('producto: ', this.product);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
      
    }

  }

  async updateProduct() {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(this.credentials);
    this.authService.post(this.UPDATE_PRODUCT_URL+this.product_id, this.credentials, token)
    .then(data =>{
      this.presentAlert("producto actualizado");
      this.navCtrl.back();
      console.log(data);
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });
  }


  async getCategories() {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null){
      this.navCtrl.navigateForward(['login']);
    }else{

      const data = this.authService.get(this.authService.baseURL + "categories", token)
      .then(data =>{
        this.categories = data.data.categories;
        console.log(this.categories);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
      
    }
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }



  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.route.queryParams.subscribe(params => {
            try{
              this.product_id = JSON.parse(params['product_id']);
              console.log(this.product_id);
              this.getValueProduct();
            }catch(ex){
              console.log(ex);
            }
          });
      
          this.getCategories();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }



  ngOnInit() {
    this.getToken();
    
  }

  ionViewWillEnter() {
		this.getToken();
	}

  async validator() {
    if(this.credentials.name.length < 3) {
      this.presentAlert("Campo nombre requerido");
    }else{
      if(this.credentials.quantity.length < 1) {
        this.presentAlert("Ingrese cantidad de productos existentes");
      }else{
        if(this.credentials.price.length < 1) {
          this.presentAlert("Ingrese el precio del producto");
        }else{
          if(this.credentials.quantity.length < 1 ) {
            this.presentAlert("seleccione una foto");
          }else{
            if(this.credentials.title.length < 3) {
              this.presentAlert("Campo titulo es requerido");
            }else{
              if(this.credentials.description.length < 3){
                this.presentAlert("Campo descripción es requerido");
              }else{
                if(this.credentials.category_id == 0){
                  this.presentAlert("Por favor seleccione una categoria");
                }else{
                  if(this.credentials.offer.length < 1) {
                    this.presentAlert("Por favor ingrese la oferta si no tiene agregue un 0");
                  }else{
                    console.log("formulario completo");
                    this.updateProduct();
                  }
                 
                }
              }
            }
          }
        }
      }
    }
  }


  desktopGallery( $event ) {    
    let file   = $event.target.files[0];
    let reader = new FileReader();

    reader.onloadend = () => {
      this.credentials.picture = "" + reader.result;
    }
    reader.readAsDataURL(file);
  }

  openGalery() {
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth  : 500,
      targetHeight : 500,
      quality      : 100,
      mediaType    : this.camera.MediaType.PICTURE,
      sourceType   : this.camera.PictureSourceType.PHOTOLIBRARY
    }
    this.camera.getPicture(options).then(imageData => {
      this.product[0].picture = `data:image/jpeg;base64,${imageData}`;
    }).catch(error => {
      console.error(error);
    });
  }

  openCamera(){
    const options: CameraOptions = {
      quality         : 100,
      destinationType : this.camera.DestinationType.DATA_URL,
      encodingType    : this.camera.EncodingType.JPEG,
      mediaType       : this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then((imageData) => {
      this.product[0].picture = `data:image/jpeg;base64,${imageData}`;
    }, (err) => {
      console.log(err)
    });
  }

}
