import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DomiciliaryPage } from './domiciliary.page';

describe('DomiciliaryPage', () => {
  let component: DomiciliaryPage;
  let fixture: ComponentFixture<DomiciliaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomiciliaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DomiciliaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
