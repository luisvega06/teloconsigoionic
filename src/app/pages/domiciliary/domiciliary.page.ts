import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import Axios from 'axios';

@Component({
  selector: 'app-domiciliary',
  templateUrl: './domiciliary.page.html',
  styleUrls: ['./domiciliary.page.scss'],
})
export class DomiciliaryPage implements OnInit {

  domiciaries = [];
  DOMICILIARY_STATE_URL = this.authService.baseURL +"domiciliary/actualizar/"
  state = 2;

  constructor( public navCtrl:  NavController,
    private  authService :  AuthserviceService,
    public alertController   :  AlertController,
    private jwtService: JwtService ) { }

  ngOnInit() {
    this.getDomiciliary();
  }

  ionViewWillEnter() {
    this.getDomiciliary();
  }

  async createDomiciliary() {
    this.navCtrl.navigateForward(['create-domiciliary']);
  }

  async getDomiciliary() {
    const token = localStorage.getItem('TokenTeloConsigo');
    const data = this.authService.get(this.authService.baseURL + "domiciliario/order/"+this.state, token)
    .then(data =>{
      this.domiciaries = data.data.domiciliaries;
      console.log(this.domiciaries);
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
      //this.presentAlert(error.response.data.data)
    });
  }

  async updateDomiciliary(id, newstate) {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(id, newstate);
    this.authService.post(this.authService.baseURL + 'domiciliary/actualizar/' +id+"/"+newstate, this.domiciaries, token)
    .then(data =>{
      //this.refresh();
      this.getDomiciliary();
      console.log(data);
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
    });
  }


}
