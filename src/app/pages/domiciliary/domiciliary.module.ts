import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DomiciliaryPageRoutingModule } from './domiciliary-routing.module';

import { DomiciliaryPage } from './domiciliary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DomiciliaryPageRoutingModule
  ],
  declarations: [DomiciliaryPage]
})
export class DomiciliaryPageModule {}
