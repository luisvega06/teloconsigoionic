import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DomiciliaryPage } from './domiciliary.page';

const routes: Routes = [
  {
    path: '',
    component: DomiciliaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DomiciliaryPageRoutingModule {}
