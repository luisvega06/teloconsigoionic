import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  constructor(
    private route: ActivatedRoute,
    public navCtrl: NavController,
    private  authService :  AuthserviceService,
    private jwtService: JwtService
  ) { }

  category;
  user: any = {};
  products : any = [
    
  ];

  async getProducts() {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null){
      this.navCtrl.navigateForward(['login']);
    }else{
      const data = this.authService.get(this.authService.baseURL + "product/"+this.category, token)
      .then(data =>{
        this.products = data.data.products;
      console.log(this.products);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
      
    }
    
  }

  async updateProduct(id) {

    console.log(id);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        product_id: JSON.stringify(id)
      }
    };
    this.navCtrl.navigateForward(['/edit-product'], navigationExtras);
    
  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.route.queryParams.subscribe(params => {
            try{
              this.category = JSON.parse(params['category']);
              console.log(this.category);
              this.getProducts();
            }catch(ex){
              console.log(ex);
            }
          }); 
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }
  
  ngOnInit() {
    this.getToken();
  }
  ionViewWillEnter() {
    this.getToken();
	}
}
