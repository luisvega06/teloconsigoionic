import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';
import { tokenName } from '@angular/compiler';

@Component({
  selector: 'app-manage-order',
  templateUrl: './manage-order.page.html',
  styleUrls: ['./manage-order.page.scss'],
})
export class ManageOrderPage implements OnInit {

  user: any = {};
  idOrder;
  order = [];
  userDomiciliary = [];
  credentials = {
    domiciliary_id: 0
  };
  URL_SEND_ORDER = this.authService.baseURL +"orders/update-state-order/"
  domiOk =[];
  sendOk = [];
  stateNew = 1;

  constructor( private route: ActivatedRoute,
               public alertController   :  AlertController,
               public navCtrl            :  NavController,
               private  authService            :  AuthserviceService,
               private jwtService: JwtService ) { }



  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async sendOrder() {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(this.order[0].reservations);
    this.authService.post(this.authService.baseURL + 'orders/update-state-order/' +this.idOrder+"/"+this.stateNew , this.order, token)
    .then(data =>{
      //localStorage.setItem("TokenTigoView",data.data.token);
      this.navCtrl.navigateForward(['orders-management']);
      //console.log(data);
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });
    //const data = await axios.get("http://127.0.0.1:8000/api/orders/update-state-order/"+this.idOrder+"/"+this.stateNew);
    //this.sendOk = data.data.order;
    
    
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmación!',
      message: ' <strong>¿Desea enviar esta orden?</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Enviar',
          handler: () => {
          console.log('Confirm Okay');
          this.sendOrder();
          }
        }
      ]
    });

    await alert.present();
  }

  async getOder(id) {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null){
      this.navCtrl.navigateForward(['login']);
    }else{
      const data = this.authService.get(this.authService.baseURL + "orders/order/"+id, token)
      .then(data =>{
        this.order = data.data.orders;
        //console.log(this.order);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
      
    }
    
  }

  async assignDomiciliary() {
    const token = localStorage.getItem('TokenTeloConsigo');
    const data = this.authService.get(this.authService.baseURL + "orders/domiciliary/"+this.idOrder+"/"+this.credentials.domiciliary_id, token)
    .then(data =>{
      this.domiOk = data.data.order;
      //this.getOder(this.domiOk);
      this.refresh();
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
      //this.presentAlert(error.response.data.data)
    });
    
  }
  refresh(): void {
    window.location.reload();
  }

  async validatorDomiciliary() {
    if(this.credentials.domiciliary_id == 0) {
      this.presentAlert("Por favor eliga el domiciliario");
    }else{
      this.assignDomiciliary();
    }
  }

  async getDomiciliary() {
    const token = localStorage.getItem('TokenTeloConsigo');
    
      const data = this.authService.get(this.authService.baseURL + "domiciliary", token)
      .then(data =>{
        this.userDomiciliary = data.data.domiciliary;
        //console.log(this.userDomiciliary);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
      
    
    
  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.route.queryParams.subscribe(params => {
            try{
              this.idOrder = JSON.parse(params['orderId']);
              console.log(this.idOrder);
              this.getOder(this.idOrder);
            }catch(ex){
              console.log(ex);
            }
          });
          this.getDomiciliary();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  ngOnInit() {
    this.getToken();
  }

  ionViewWillEnter() {
    this.getToken();
	}


  addRemove(symbol:string ='+', )
	{
		
	}

}
