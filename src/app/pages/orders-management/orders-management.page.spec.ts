import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrdersManagementPage } from './orders-management.page';

describe('OrdersManagementPage', () => {
  let component: OrdersManagementPage;
  let fixture: ComponentFixture<OrdersManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersManagementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrdersManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
