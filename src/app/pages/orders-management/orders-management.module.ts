import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersManagementPageRoutingModule } from './orders-management-routing.module';

import { OrdersManagementPage } from './orders-management.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdersManagementPageRoutingModule
  ],
  declarations: [OrdersManagementPage]
})
export class OrdersManagementPageModule {}
