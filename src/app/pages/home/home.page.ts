import { Component, OnInit } from '@angular/core';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import { NavController } from '@ionic/angular';
import axios from 'axios';
import { MenuController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  user: any = {};
  URL_UPDATE_STATE = this.authService.baseURL +"publication/updateState/"
  textoBuscar = '';
  publications : any = [
    
  ];
  countries : any = [
    
  ];

  constructor( private menuCtrl: MenuController,
              private jwtService: JwtService,
              private  authService :  AuthserviceService,
              public navCtrl            :  NavController,
              public alertController: AlertController,
              private appComnentCrlt: AppComponent ) {}

  
  async getPublications() {
    const token = localStorage.getItem('TokenTeloConsigo');
    const data = this.authService.get(this.authService.baseURL + "publicationsAdmin", token)
    .then(data =>{
      this.publications = data.data.publications;
      console.log(this.publications);
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
      //this.presentAlert(error.response.data.data)
    });

  }

  

  formatNumber( valor : number = 0 ){
		return new Intl.NumberFormat("de-DE").format( valor );
	}

  async menuUpdate() {
    this.appComnentCrlt.getToken();
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.getPublications();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  buscar( event ) {
    console.log(event.detail.value)
    this.textoBuscar = event.detail.value;
  }

  async newStatePublication(id, newState) {
    console.log(id, newState);
    const token = localStorage.getItem('TokenTeloConsigo');
    this.authService.get(this.authService.baseURL + 'publication/updateState/' +id+"/"+newState, token)
    .then(data =>{
      this.presentAlert("El estado se ha cambiado correctamente");
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
      //this.presentAlert(error.response.data.data)
    });
  }

  ngOnInit() {
    this.getToken();
    
    //this.getPublications();
  }

  ionViewWillEnter() {
    this.getToken();
    this.menuUpdate();
	}

  

}
