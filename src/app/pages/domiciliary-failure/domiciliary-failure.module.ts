import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DomiciliaryFailurePageRoutingModule } from './domiciliary-failure-routing.module';

import { DomiciliaryFailurePage } from './domiciliary-failure.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DomiciliaryFailurePageRoutingModule
  ],
  declarations: [DomiciliaryFailurePage]
})
export class DomiciliaryFailurePageModule {}
