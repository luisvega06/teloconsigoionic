import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DomiciliaryFailurePage } from './domiciliary-failure.page';

describe('DomiciliaryFailurePage', () => {
  let component: DomiciliaryFailurePage;
  let fixture: ComponentFixture<DomiciliaryFailurePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomiciliaryFailurePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DomiciliaryFailurePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
