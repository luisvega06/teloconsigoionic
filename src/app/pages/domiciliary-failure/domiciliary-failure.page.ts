import { Component, OnInit } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';

@Component({
  selector: 'app-domiciliary-failure',
  templateUrl: './domiciliary-failure.page.html',
  styleUrls: ['./domiciliary-failure.page.scss'],
})
export class DomiciliaryFailurePage implements OnInit {

  ordersPendings = [];
	pendiente = 3;
	count:number=0;
	stateSend = 3;
	user: any = {};
	constructor( public navCtrl: NavController,
				 private  authService :  AuthserviceService,
				 private jwtService: JwtService ) 
	{

	}
	
	ngOnInit() 
	{
		this.getToken();
	}

	ionViewWillEnter() {
		this.getToken();
	}
	

	getToken() {
		try{
		  const token = localStorage.getItem('TokenTeloConsigo');
		  if(token == null){
			this.navCtrl.navigateForward(['login']);
		  }else{
			console.log(token);
			this.user = this.jwtService.parseJwt(token);
			if(this.user.rol == 3){
			  this.getOrdespending();
			}else{
        if(this.user.rol == 1){
          this.navCtrl.navigateForward(['home']);
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
			  
			}
		  }
		  
		  console.log(this.user);
		}catch( ex){
		  console.log(ex);
		}
	  }



	async getOrdespending() {
		const token = localStorage.getItem('TokenTeloConsigo');
		if(token == null){
			this.navCtrl.navigateForward(['login']);
		}else{
      //domiciliario/my-orders/{id}/{state}
			const data = this.authService.get(this.authService.baseURL + "domiciliario/my-orders/"+this.user.id+"/"+this.stateSend, token)
			.then(data =>{
				this.ordersPendings = data.data.orders;
				console.log(this.ordersPendings);
				//console.log(data);
			  })
			  .catch(error => {
				console.log(error.response);
				this.navCtrl.navigateForward(['login']);
				//this.presentAlert(error.response.data.data)
			  });
			
		}
		
	}
	

	async manageOrder(id) {
		console.log(id);
		let navigationExtras: NavigationExtras = {
		  queryParams: {
			orderId: JSON.stringify(id)
		  }
		};
		this.navCtrl.navigateForward(['/orden-pending-d'], navigationExtras);
	}

	addRemove(symbol:string ='+')
	{
		if (symbol=='+') {
			this.count++;
		}else{
			if (this.count - 1 > 0) {
				this.count--;
			}
		}
	}

}
