import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DomiciliaryFailurePage } from './domiciliary-failure.page';

const routes: Routes = [
  {
    path: '',
    component: DomiciliaryFailurePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DomiciliaryFailurePageRoutingModule {}
