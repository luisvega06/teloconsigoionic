import { Component, OnInit } from '@angular/core';
import { JwtService } from '../../jwt.service';
import { NavController } from '@ionic/angular';
import { AuthserviceService } from '../../authservice.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import axios from 'axios';

@Component({
  selector: 'app-create-publication',
  templateUrl: './create-publication.page.html',
  styleUrls: ['./create-publication.page.scss'],
})
export class CreatePublicationPage implements OnInit {

  customAlertOptions: any = {
    header: 'Productos disponibles',
    subHeader: 'Selecciona un producto',
    translucent: true
  };

  user: any = {};

  products : any = [
   
  ];
  REGISTER_URL = this.authService.baseURL + 'publications';
  credentials = {
    title: "",
    description : "",
    photo: "photo",
    product_id: 0,
    user_id: "1",
    state: "1"
  };


  constructor( private  authService      :  AuthserviceService,
               public alertController    :  AlertController,
               private jwtService: JwtService,
               public navCtrl            :  NavController,
               private router: Router ) { }

  async getProducts() {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null){
      this.navCtrl.navigateForward(['login']);
    }else{
      const data = this.authService.get(this.authService.baseURL + "products", token)
      .then(data =>{
        this.products = data.data.products;
        console.log(this.products);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
      
    }
    
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async createPublication() {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(this.credentials);
    this.authService.post(this.REGISTER_URL, this.credentials, token)
    .then(data =>{
      //localStorage.setItem("TokenTigoView",data.data.token);
      this.presentAlert("Publicación exitosa");
      this.router.navigate(['/home']);
      console.log(data);
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
    });

  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.getProducts();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  ngOnInit() {
    this.getToken();
  }
  ionViewWillEnter() {
    this.getToken();
  }

  async validator() {
    if(this.credentials.title.length < 3){
      this.presentAlert("Campo Titulo requerido");
    }else{
      if(this.credentials.description.length < 5){
        this.presentAlert("Campo descripción requerido");
      }else{
        if(this.credentials.photo.length < 1){
          this.presentAlert("Seleccione una foto");
        }else{
          if(this.credentials.product_id == 0){
            this.presentAlert("Debe elegir un producto");
          }else{
            this.createPublication();
          }
        }
      }
    }
  }

}
