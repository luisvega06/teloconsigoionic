import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderUndeliveredPageRoutingModule } from './order-undelivered-routing.module';

import { OrderUndeliveredPage } from './order-undelivered.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderUndeliveredPageRoutingModule
  ],
  declarations: [OrderUndeliveredPage]
})
export class OrderUndeliveredPageModule {}
