import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderUndeliveredPage } from './order-undelivered.page';

describe('OrderUndeliveredPage', () => {
  let component: OrderUndeliveredPage;
  let fixture: ComponentFixture<OrderUndeliveredPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderUndeliveredPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderUndeliveredPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
