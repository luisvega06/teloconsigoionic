import { Component, OnInit } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';

@Component({
  selector: 'app-order-undelivered',
  templateUrl: './order-undelivered.page.html',
  styleUrls: ['./order-undelivered.page.scss'],
})
export class OrderUndeliveredPage implements OnInit {
  idOrder;
  order =[];
  user: any = {};

  constructor( public navCtrl: NavController,
              private route: ActivatedRoute,
              private  authService            :  AuthserviceService,
              private jwtService: JwtService ) { }

  ngOnInit() {
    this.getToken();
  }
  ionViewWillEnter() {
    this.getToken();
	}

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.route.queryParams.subscribe(params => {
            try{
              this.idOrder = JSON.parse(params['orderId']);
              console.log(this.idOrder);
              this.getOrder(this.idOrder);
            }catch(ex){
              console.log(ex);
            }
          });
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  async getOrder(id) {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null){
      this.navCtrl.navigateForward(['login']);
    }else{
      const data =this.authService.get(this.authService.baseURL + "orders/order/"+id, token)
      .then(data =>{
        this.order = data.data.orders;
        console.log(this.order);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });

    }
  }


}
