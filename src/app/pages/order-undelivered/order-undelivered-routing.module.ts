import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderUndeliveredPage } from './order-undelivered.page';

const routes: Routes = [
  {
    path: '',
    component: OrderUndeliveredPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderUndeliveredPageRoutingModule {}
