import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegisterLiderPage } from './register-lider.page';

describe('RegisterLiderPage', () => {
  let component: RegisterLiderPage;
  let fixture: ComponentFixture<RegisterLiderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterLiderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterLiderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
