import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterLiderPage } from './register-lider.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterLiderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterLiderPageRoutingModule {}
