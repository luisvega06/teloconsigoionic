import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterLiderPageRoutingModule } from './register-lider-routing.module';

import { RegisterLiderPage } from './register-lider.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterLiderPageRoutingModule
  ],
  declarations: [RegisterLiderPage]
})
export class RegisterLiderPageModule {}
