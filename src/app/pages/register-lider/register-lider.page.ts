import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../../authservice.service';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import axios from 'axios';
import { JwtService } from '../../jwt.service';


@Component({
  selector: 'app-register-lider',
  templateUrl: './register-lider.page.html',
  styleUrls: ['./register-lider.page.scss'],
})
export class RegisterLiderPage implements OnInit {

  hideInputPassword:boolean=true;

  categories = [];
  
  respuesta = "Registro con exito";
  campos = "campos vacios";
  credentials = {
    name: "",
    phone: "",
    email: "",
    password: "",
    direction: "",
    state: 1,
    rol_id: "7",
    user_id: '',
    category_id: '',
    department_id: 0,
    municipality_id: 0
  };
  departments = [];
  municipalitis = [];
  x = 0;
  REGISTER_URL = this.authService.baseURL + 'users';
  constructor(private  authService      :  AuthserviceService,
              public alertController    :  AlertController,
              private jwtService: JwtService,
              public loadingController: LoadingController,
              private router: Router) { }

  ngOnInit() {
    console.log(this.credentials);
    
    
    this.getDepartment();
    this.getCategories();
  }

  async getDepartment() {
    const data = this.authService.get(this.authService.baseURL + "department", {})
    .then(data =>{
      this.departments = data.data.departments;
    })
    .catch(error => {
      console.log(error.response);
      
      //this.presentAlert(error.response.data.data)
    });
  }

  async getMunicipality(municipality_id) {
    const data = await axios.get(this.authService.baseURL + "municipality/"+municipality_id);
    this.municipalitis = data.data.municipality
    console.log(this.municipalitis);
  }

  buscar( event ) {
    console.log(event.detail.value)
    this.x = event.detail.value;
    console.log(this.x);
    this.getMunicipality(this.x);

  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 1000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async create() {
    const token = localStorage.getItem('TokenTeloConsigo');
    let user = this.jwtService.parseJwt(token);
    this.credentials.user_id = user.id
    console.log(this.credentials);
    this.presentLoading();
    this.authService.post(this.authService.baseURL +  'users', this.credentials, {})
    .then(data =>{
      //localStorage.setItem("TokenTigoView",data.data.token);
      
      console.log(data);
      if(data.data.user == "ya existe"){
        this.presentAlert("este celular ya se encuentra registrado");
      }else{
        this.presentAlert(this.respuesta);
        this.router.navigate(['/lider']);
      }
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });
  }

  async validator() {
    if(this.credentials.name.length == 0){
      this.presentAlert("Ingrese el nombre ");
    }else{
      if(this.credentials.phone.length == 0){
        this.presentAlert("Ingrese un celular de contacto");
      }else{
        if(this.credentials.state == 0){
          this.presentAlert("el estado no esta definido");
        }else{
          if(this.credentials.email.length < 6){
            this.presentAlert("Por favor ingrese un correo valido");
          }else{
            if(this.credentials.direction.length < 4){
              this.presentAlert("Po favor ingrese un dirección valida");
            }else{
              if(this.credentials.password.length == 0){
                this.presentAlert("Por favor ingrese una contraseña");
              }else{
                if(this.credentials.municipality_id == 0){
                  this.presentAlert("elije tu ubicación");
                }else{
                  this.create();
                }
              }
            }
          }
        }
      }
    }
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async validate() {
    if(this.credentials.name.length < 1 ){
      this.presentAlert(this.campos)
    }else{
      this.create();
    }
  }


  async getCategories() {
    this.authService.get(this.authService.baseURL + 'categories', {})
      .then(data => {
        this.categories = data.data.categories;        
      })
  }

}
