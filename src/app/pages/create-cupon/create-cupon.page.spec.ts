import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateCuponPage } from './create-cupon.page';

describe('CreateCuponPage', () => {
  let component: CreateCuponPage;
  let fixture: ComponentFixture<CreateCuponPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCuponPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateCuponPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
