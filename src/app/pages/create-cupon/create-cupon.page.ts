import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';

@Component({
  selector: 'app-create-cupon',
  templateUrl: './create-cupon.page.html',
  styleUrls: ['./create-cupon.page.scss'],
})
export class CreateCuponPage implements OnInit {

  user: any = {};

  credentials = {
    code: "",
    price: 0,
    state: 0
  };

  CREAR_CUPON_URL = this.authService.baseURL +"discount_vounchers"

  constructor( public alertController:  AlertController,
               private  authService :  AuthserviceService,
               public navCtrl       :  NavController,
               private jwtService: JwtService ) { }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }


  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmación!',
      message: ' <strong>¿Esta seguro de crear este cupon?</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si, crearlo',
          handler: () => {
          console.log('Confirm Okay');
          this.createCupon();
          }
        }
      ]
    });

    await alert.present();
  }

  async createCupon() {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(this.credentials);
    this.authService.post(this.authService.baseURL + 'discount_vounchers', this.credentials, token)
    .then(data =>{
      this.navCtrl.navigateForward(['discount-cupon']);
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });
  }

  async validator() {
    if(this.credentials.code.length == 0){
      this.presentAlert("El codigo del cupon es obligatorio");
    }else{
      if(this.credentials.price < 1){
        this.presentAlert("Ingrese un monto valido por favor");
      }else{
        if(this.credentials.state == 0){
          this.presentAlert("Por favor eliga un estado inicial de su cupon");
        }else{
          console.log("cupon creado");
          this.presentAlertConfirm();
        }
      }
    }
  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          //this.getPublications();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  ngOnInit() {
    this.getToken();
  }
  ionViewWillEnter() {
    this.getToken();
  }

}
