import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateCuponPage } from './create-cupon.page';

const routes: Routes = [
  {
    path: '',
    component: CreateCuponPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateCuponPageRoutingModule {}
