import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateCuponPageRoutingModule } from './create-cupon-routing.module';

import { CreateCuponPage } from './create-cupon.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateCuponPageRoutingModule
  ],
  declarations: [CreateCuponPage]
})
export class CreateCuponPageModule {}
