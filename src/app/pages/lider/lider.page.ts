import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../../authservice.service';
import { JwtService } from '../../jwt.service';


@Component({
  selector: 'app-lider',
  templateUrl: './lider.page.html',
  styleUrls: ['./lider.page.scss'],
})
export class LiderPage implements OnInit {

  constructor( private  authService      :  AuthserviceService, private jwtService: JwtService) { }

  asesores = [];
  active = 0;

  ngOnInit() {
    const token = localStorage.getItem('TokenTeloConsigo');
    let user = this.jwtService.parseJwt(token);
    this.authService.get(this.authService.baseURL + 'get-users/' + user.id)
      .then(data => {
        let _asesores = data.data[0].users;
        _asesores.forEach(element => {
          let data = {
            name: element.name,
            id: element.id,
            category: element.category ? element.category.name : ''
          }
          this.asesores.push(data);          
        });
        console.log('hola' ,this.asesores);
      })
  }



}
