import { Component, OnInit } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { JwtService } from '../../jwt.service';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';



@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  user: any = {};

  categories : any = [
    
  ];

  constructor(  public navCtrl: NavController,
                private  authService :  AuthserviceService,
                private jwtService: JwtService,
                private router: Router ) { }

  async getCategory() {
    const token = localStorage.getItem('TokenTeloConsigo');
   
    const data = this.authService.get(this.authService.baseURL + "categories", token)
    .then(data =>{
      this.categories = data.data.categories;
      console.log(this.categories);
    })
    .catch(error => {
      console.log(error.response);
      this.navCtrl.navigateForward(['login']);
      //this.presentAlert(error.response.data.data)
    });
    
    
  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.getCategory();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  async getProducts(id) {
    console.log(id);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        category: JSON.stringify(id)
      }
    };
    this.navCtrl.navigateForward(['/product'], navigationExtras);
  }

  ngOnInit() {
    this.getToken();
  }
  ionViewWillEnter() {
    this.getToken();
  }

}
