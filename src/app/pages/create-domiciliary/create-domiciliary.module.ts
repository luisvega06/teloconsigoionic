import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateDomiciliaryPageRoutingModule } from './create-domiciliary-routing.module';

import { CreateDomiciliaryPage } from './create-domiciliary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateDomiciliaryPageRoutingModule
  ],
  declarations: [CreateDomiciliaryPage]
})
export class CreateDomiciliaryPageModule {}
