import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateDomiciliaryPage } from './create-domiciliary.page';

describe('CreateDomiciliaryPage', () => {
  let component: CreateDomiciliaryPage;
  let fixture: ComponentFixture<CreateDomiciliaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDomiciliaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateDomiciliaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
