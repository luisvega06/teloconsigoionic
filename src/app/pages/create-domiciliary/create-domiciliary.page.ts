import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import axios from 'axios';

@Component({
  selector: 'app-create-domiciliary',
  templateUrl: './create-domiciliary.page.html',
  styleUrls: ['./create-domiciliary.page.scss'],
})
export class CreateDomiciliaryPage implements OnInit {

  user: any = {};
  departments = [];
  municipalitis = [];
  x = 0;
  credentials = {
    name: "",
    phone: "",
    direction: "",
    email: "",
    password: "secret",
    rol_id: "3",
    department_id: "",
    municipality_id: "",
    state: 0
  };

  CREAR_DOMICILIARY_URL = this.authService.baseURL +"users"
  

  constructor( public alertController:  AlertController,
    private  authService :  AuthserviceService,
    public navCtrl       :  NavController,
    private jwtService: JwtService ) { }


  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async getDepartment() {
    const data = this.authService.get(this.authService.baseURL + "department", {})
    .then(data =>{
      this.departments = data.data.departments;
      console.log(this.departments);
    })
    .catch(error => {
      console.log(error.response);
      
      //this.presentAlert(error.response.data.data)
    });
  }

  async getMunicipality(municipality_id) {
    const data = await axios.get(this.authService.baseURL + "municipality/"+municipality_id);
    this.municipalitis = data.data.municipality
    console.log(this.municipalitis);
  }

  buscar( event ) {
    console.log(event.detail.value)
    this.x = event.detail.value;
    console.log(this.x);
    this.getMunicipality(this.x);

  }


  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmación!',
      message: ' <strong>¿Esta seguro de crear este domiciliario?</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si, crearlo',
          handler: () => {
          console.log('Confirm Okay');
          this.createDomiciliary();
          }
        }
      ]
    });

    await alert.present();
  }

  async validator() {
    if(this.credentials.name.length == 0){
      this.presentAlert("Ingrese el nombre del domiciliario");
    }else{
      if(this.credentials.phone.length == 0){
        this.presentAlert("Ingrese un celular de contacto");
      }else{
        if(this.credentials.state == 0){
          this.presentAlert("Por favor eliga un estado inicial del domiciliario");
        }else{
          console.log("domiciliario creado");
          this.presentAlertConfirm();
        }
      }
    }
  }


  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          //this.getPublications();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }


  async createDomiciliary() {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(this.credentials);
    this.authService.post(this.CREAR_DOMICILIARY_URL, this.credentials, token)
    .then(data =>{
      this.navCtrl.navigateForward(['domiciliary']);
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });
  }

  ngOnInit() {
    this.getDepartment();

  }

  ionViewWillEnter() {
    this.getDepartment();
  }


}
