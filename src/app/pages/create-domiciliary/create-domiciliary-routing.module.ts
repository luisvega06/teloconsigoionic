import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateDomiciliaryPage } from './create-domiciliary.page';

const routes: Routes = [
  {
    path: '',
    component: CreateDomiciliaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateDomiciliaryPageRoutingModule {}
