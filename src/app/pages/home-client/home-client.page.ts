import { Component, OnInit } from '@angular/core';
import { JwtService } from '../../jwt.service';
import { AlertController } from '@ionic/angular';
import { AuthserviceService } from '../../authservice.service';
import { NavController } from '@ionic/angular';
import { AppComponent } from '../../app.component';
import axios from 'axios';

@Component({
  selector: 'app-home-client',
  templateUrl: './home-client.page.html',
  styleUrls: ['./home-client.page.scss'],
})
export class HomeClientPage implements OnInit {

  user: any = {};
  form: any = {
    user_id: "",
    product_id: "",
    quantity: 1,
    state: 0
  };
  existe = "";
  textoBuscar = '';
  temporal = 10000;
  number;
  publications : any = [
    
  ];
 
  RESERVATIONS_URL = this.authService.baseURL +'reservations';

  constructor( private jwtService: JwtService,
               private  authService :  AuthserviceService,
               public alertController    :  AlertController,
               public navCtrl            :  NavController,
               private appComnentCrlt: AppComponent ) { }

  async getPublications() {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null) {
      this.navCtrl.navigateForward(['login']);
    }else{
      const data = this.authService.get(this.authService.baseURL + "publications", token)
      .then(data =>{
        this.publications = data.data.publications;
        console.log(this.publications);
        
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
      //this.publications = data.data.publications;
      //console.log(this.publications);
    }
    
    
  }

  formatNumber( valor : number = 0 ){
		return new Intl.NumberFormat("de-DE").format( valor );
	}

  async menuUpdate() {
    this.appComnentCrlt.getToken();
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }


  async setReservation(id, product) {
    const token = localStorage.getItem('TokenTeloConsigo');
    this.form.user_id = this.user.id;
    this.form.product_id = id;
    console.log(this.form);
    this.authService.post(this.authService.baseURL + 'reservations', this.form, token)
    .then(data =>{
      this.existe = data.data.message;

      //console.log(this.existe);
      if(this.existe == "reserva existe") {
        this.presentAlert(product +", este producto ya esta agregado a tu carrito");
      }else{
        //localStorage.setItem("TokenTigoView",data.data.token);
        this.presentAlert(product +", se agrego a tu carrito de pedidos");
        console.log(data);
      }
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });
  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');

      //console.log(token);
      // console.log(this.parseJwt(token));
      this.user = this.jwtService.parseJwt(token);
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  buscar( event ) {
    console.log(event.detail.value)
    this.textoBuscar = event.detail.value;
  }

  ngOnInit() {
    this.getPublications();
    this.getToken();
  }

  ionViewWillEnter() {
		this.getPublications();
    this.getToken();
    this.menuUpdate();
	}

}
