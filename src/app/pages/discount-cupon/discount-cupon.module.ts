import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiscountCuponPageRoutingModule } from './discount-cupon-routing.module';

import { DiscountCuponPage } from './discount-cupon.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiscountCuponPageRoutingModule
  ],
  declarations: [DiscountCuponPage]
})
export class DiscountCuponPageModule {}
