import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiscountCuponPage } from './discount-cupon.page';

describe('DiscountCuponPage', () => {
  let component: DiscountCuponPage;
  let fixture: ComponentFixture<DiscountCuponPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountCuponPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiscountCuponPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
