import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiscountCuponPage } from './discount-cupon.page';

const routes: Routes = [
  {
    path: '',
    component: DiscountCuponPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiscountCuponPageRoutingModule {}
