import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { JwtService } from '../../jwt.service';
import { AuthserviceService } from '../../authservice.service';
import Axios from 'axios';

@Component({
  selector: 'app-discount-cupon',
  templateUrl: './discount-cupon.page.html',
  styleUrls: ['./discount-cupon.page.scss'],
})
export class DiscountCuponPage implements OnInit {

  CUPON_STATE_URL = this.authService.baseURL + "discount-vouncher/actualizar/"

  cupones = [];
  user: any = {};

  constructor( public navCtrl:  NavController,
              private  authService :  AuthserviceService,
              public alertController   :  AlertController,
              private jwtService: JwtService ) { }

  async getCupones() {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null){
      this.navCtrl.navigateForward(['login']);
    }else{
      const data = this.authService.get(this.authService.baseURL + "discount_vounchers", token)
      .then(data =>{
        this.cupones = data.data.discountVouncher;
        console.log(this.cupones);
      })
      .catch(error => {
        console.log(error.response);
        this.navCtrl.navigateForward(['login']);
        //this.presentAlert(error.response.data.data)
      });
    }
  
  }
  
  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  refresh(): void {
    window.location.reload();
  }

  async createCupon() {
    this.navCtrl.navigateForward(['create-cupon']);
  }

  async updateCupon(id, state) {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(id,state);
    this.authService.post(this.CUPON_STATE_URL+id+"/"+state, this.cupones, token)
    .then(data =>{
      //localStorage.setItem("TokenTigoView",data.data.token);
      //this.refresh();
      this.getCupones();
      console.log(data);
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });
  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          this.getCupones();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  ngOnInit() {
    this.getToken();
  }

  ionViewWillEnter() {
    this.getToken();
  }


}
