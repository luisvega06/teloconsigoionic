import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdminLiderPage } from './admin-lider.page';

describe('AdminLiderPage', () => {
  let component: AdminLiderPage;
  let fixture: ComponentFixture<AdminLiderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLiderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdminLiderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
