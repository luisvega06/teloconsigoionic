import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminLiderPageRoutingModule } from './admin-lider-routing.module';

import { AdminLiderPage } from './admin-lider.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminLiderPageRoutingModule
  ],
  declarations: [AdminLiderPage]
})
export class AdminLiderPageModule {}
