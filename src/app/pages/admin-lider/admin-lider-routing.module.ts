import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminLiderPage } from './admin-lider.page';

const routes: Routes = [
  {
    path: '',
    component: AdminLiderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminLiderPageRoutingModule {}
