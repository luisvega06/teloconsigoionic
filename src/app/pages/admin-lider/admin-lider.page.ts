import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../../authservice.service';
import { JwtService } from '../../jwt.service';

@Component({
  selector: 'app-admin-lider',
  templateUrl: './admin-lider.page.html',
  styleUrls: ['./admin-lider.page.scss'],
})
export class AdminLiderPage implements OnInit {

  constructor(private authService: AuthserviceService, private jwtService: JwtService) { }

  lideres = [];
  asesores = [];
  active = 0;

  ngOnInit() {
    const token = localStorage.getItem('TokenTeloConsigo');
    let user = this.jwtService.parseJwt(token);
    this.authService.get(this.authService.baseURL + 'get-leaders/')
      .then(data => {
        let _lideres = data.data;

        this.lideres = _lideres;
      });
  }


  getAsesores(id){
    this.authService.get(this.authService.baseURL + 'get-users/' + id)
    .then(data => {
      let _asesores = data.data[0].users;
      this.asesores = [];
      _asesores.forEach(element => {
        let data = {
          name: element.name,
          id: element.id,
          category: element.category ? element.category.name : ''
        }
        this.asesores.push(data);   
        this.active = id;       
      });
      console.log('hola' ,this.asesores);
    })
    
  }
}


