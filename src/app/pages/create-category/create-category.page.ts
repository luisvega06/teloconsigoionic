import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AuthserviceService } from '../../authservice.service';
import { JwtService } from '../../jwt.service';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';



@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.page.html',
  styleUrls: ['./create-category.page.scss'],
})
export class CreateCategoryPage implements OnInit {

  user: any = {};

  credentials = {
    name: "",
    description: ""
  }
  REGISTER_URL = this.authService.baseURL +'categories';
  respuesta = "Categoria registrada con exito";
  nameCampo = "Campo nombre esta vacio";
  descriptionCampo = "Campo descripción esta vacio";

  constructor( public alertController    :  AlertController,
               private  authService      :  AuthserviceService,
               private jwtService: JwtService,
               public navCtrl            :  NavController,
               private router: Router ) { }

  ngOnInit() {
    this.getToken();
  }
  ionViewWillEnter() {
    this.getToken();
  }

  getToken() {
    try{
      const token = localStorage.getItem('TokenTeloConsigo');
      if(token == null){
        this.navCtrl.navigateForward(['login']);
      }else{
        console.log(token);
        this.user = this.jwtService.parseJwt(token);
        if(this.user.rol == 2){
          //this.getPublications();
        }else{
          this.navCtrl.navigateForward(['home-client']);
        }
      }
      
      console.log(this.user);
    }catch( ex){
      console.log(ex);
    }
  }

  async createCategory() {
    const token = localStorage.getItem('TokenTeloConsigo');
    console.log(this.credentials);
    this.authService.post(this.authService.baseURL + 'categories', this.credentials, token)
    .then(data =>{
      this.presentAlert(this.respuesta);
      this.router.navigate(['/create-product']);
      console.log(data);
    })
    .catch(error => {
      console.log(error.response);
      this.presentAlert(error.response.data.data)
    });
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Notification',
      subHeader: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async validator(){
    if(this.credentials.name.length < 3){
      this.presentAlert(this.nameCampo)
    }else{
      if(this.credentials.description.length < 5){
        this.presentAlert(this.descriptionCampo)
      }else{
        this.createCategory();
        console.log("campos llenos");
      }
    }
  }

}
