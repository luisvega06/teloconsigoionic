import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrdersDeliveredPage } from './orders-delivered.page';

describe('OrdersDeliveredPage', () => {
  let component: OrdersDeliveredPage;
  let fixture: ComponentFixture<OrdersDeliveredPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersDeliveredPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrdersDeliveredPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
