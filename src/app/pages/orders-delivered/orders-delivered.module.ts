import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersDeliveredPageRoutingModule } from './orders-delivered-routing.module';

import { OrdersDeliveredPage } from './orders-delivered.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdersDeliveredPageRoutingModule
  ],
  declarations: [OrdersDeliveredPage]
})
export class OrdersDeliveredPageModule {}
