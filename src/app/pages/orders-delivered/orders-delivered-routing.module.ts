import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersDeliveredPage } from './orders-delivered.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersDeliveredPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersDeliveredPageRoutingModule {}
