import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdenPendingDPage } from './orden-pending-d.page';

const routes: Routes = [
  {
    path: '',
    component: OrdenPendingDPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdenPendingDPageRoutingModule {}
