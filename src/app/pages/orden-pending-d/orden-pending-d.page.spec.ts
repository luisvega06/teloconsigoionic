import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrdenPendingDPage } from './orden-pending-d.page';

describe('OrdenPendingDPage', () => {
  let component: OrdenPendingDPage;
  let fixture: ComponentFixture<OrdenPendingDPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdenPendingDPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrdenPendingDPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
