import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdenPendingDPageRoutingModule } from './orden-pending-d-routing.module';

import { OrdenPendingDPage } from './orden-pending-d.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdenPendingDPageRoutingModule
  ],
  declarations: [OrdenPendingDPage]
})
export class OrdenPendingDPageModule {}
