import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsClientPage } from './tabs-client.page';


const routes: Routes = [
  {
    path: '',
    component: TabsClientPage,
    children: [
      {
        path: 'home-client',
        loadChildren: '../home-client/home-client.module#HomeClientPageModule'
      },
      {
        path:'my-reserve',
        loadChildren: '../my-reserve/my-reserve.module#MyReservePageModule'
      },
      {
        path: 'profile',
        loadChildren: '../profile/profile.module#ProfilePageModule'
      },
      {
        path:'create-category',
        loadChildren: '../create-category/create-category.module#CreateCategoryPageModule'
      },
      {
        path: 'my-order',
        loadChildren: '../my-order/my-order.module#MyOrderPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsClientPageRoutingModule {}
