import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabsClientPage } from './tabs-client.page';

describe('TabsClientPage', () => {
  let component: TabsClientPage;
  let fixture: ComponentFixture<TabsClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabsClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
