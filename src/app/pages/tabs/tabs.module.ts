import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router'
import { IonicModule } from '@ionic/angular';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';

import { HomePageModule } from '../home/home.module';
import { CreatePublicationPageModule } from '../create-publication/create-publication.module';
import { CreateCategoryPageModule } from '../create-category/create-category.module';
import { CreateProductPageModule } from '../create-product/create-product.module';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: '../home/home.module#HomePageModule'
      },
      {
        path:'create-publication',
        loadChildren: '../create-publication/create-publication.module#CreatePublicationPageModule'
      },
      {
        path: 'category',
        loadChildren: '../category/category.module#CategoryPageModule'
      },
      {
        path:'create-category',
        loadChildren: '../create-category/create-category.module#CreateCategoryPageModule'
      },
      {
        path: 'create-product',
        loadChildren: '../create-product/create-product.module#CreateProductPageModule'
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsPageRoutingModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: [TabsPage]
})
export class TabsPageModule {}
