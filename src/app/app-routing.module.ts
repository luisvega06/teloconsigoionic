import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'category',
    loadChildren: () => import('./pages/category/category.module').then( m => m.CategoryPageModule)
  },
  {
    path: 'create-category',
    loadChildren: () => import('./pages/create-category/create-category.module').then( m => m.CreateCategoryPageModule)
  },
  {
    path: 'create-product',
    loadChildren: () => import('./pages/create-product/create-product.module').then( m => m.CreateProductPageModule)
  },
  {
    path: 'product',
    loadChildren: () => import('./pages/product/product.module').then( m => m.ProductPageModule)
  },
  {
    path: 'create-publication',
    loadChildren: () => import('./pages/create-publication/create-publication.module').then( m => m.CreatePublicationPageModule)
  },
  {
    path: 'tabs-client',
    loadChildren: () => import('./pages/tabs-client/tabs-client.module').then( m => m.TabsClientPageModule)
  },
  {
    path: 'home-client',
    loadChildren: () => import('./pages/home-client/home-client.module').then( m => m.HomeClientPageModule)
  },
  {
    path: 'my-reserve',
    loadChildren: () => import('./pages/my-reserve/my-reserve.module').then( m => m.MyReservePageModule)
  },
  {
    path: 'my-order',
    loadChildren: () => import('./pages/my-order/my-order.module').then( m => m.MyOrderPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'orders-management',
    loadChildren: () => import('./pages/orders-management/orders-management.module').then( m => m.OrdersManagementPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'edit-product',
    loadChildren: () => import('./pages/edit-product/edit-product.module').then( m => m.EditProductPageModule)
  },
  {
    path: 'manage-order',
    loadChildren: () => import('./pages/manage-order/manage-order.module').then( m => m.ManageOrderPageModule)
  },
  {
    path: 'confirmed-orders',
    loadChildren: () => import('./pages/confirmed-orders/confirmed-orders.module').then( m => m.ConfirmedOrdersPageModule)
  },
  {
    path: 'confirm-order',
    loadChildren: () => import('./pages/confirm-order/confirm-order.module').then( m => m.ConfirmOrderPageModule)
  },
  {
    path: 'create-domiciliary',
    loadChildren: () => import('./pages/create-domiciliary/create-domiciliary.module').then( m => m.CreateDomiciliaryPageModule)
  },
  {
    path: 'domiciliary',
    loadChildren: () => import('./pages/domiciliary/domiciliary.module').then( m => m.DomiciliaryPageModule)
  },
  {
    path: 'create-cupon',
    loadChildren: () => import('./pages/create-cupon/create-cupon.module').then( m => m.CreateCuponPageModule)
  },
  {
    path: 'discount-cupon',
    loadChildren: () => import('./pages/discount-cupon/discount-cupon.module').then( m => m.DiscountCuponPageModule)
  },
  {
    path: 'undelivered-orders',
    loadChildren: () => import('./pages/undelivered-orders/undelivered-orders.module').then( m => m.UndeliveredOrdersPageModule)
  },
  {
    path: 'orders-delivered',
    loadChildren: () => import('./pages/orders-delivered/orders-delivered.module').then( m => m.OrdersDeliveredPageModule)
  },
  {
    path: 'order-detail',
    loadChildren: () => import('./pages/order-detail/order-detail.module').then( m => m.OrderDetailPageModule)
  },
  {
    path: 'order-undelivered',
    loadChildren: () => import('./pages/order-undelivered/order-undelivered.module').then( m => m.OrderUndeliveredPageModule)
  },
  {
    path: 'domiciliari-top',
    loadChildren: () => import('./pages/domiciliari-top/domiciliari-top.module').then( m => m.DomiciliariTopPageModule)
  },
  {
    path: 'pending-orders',
    loadChildren: () => import('./pages/pending-orders/pending-orders.module').then( m => m.PendingOrdersPageModule)
  },
  {
    path: 'domiciliary-delivered',
    loadChildren: () => import('./pages/domiciliary-delivered/domiciliary-delivered.module').then( m => m.DomiciliaryDeliveredPageModule)
  },
  {
    path: 'domiciliary-failure',
    loadChildren: () => import('./pages/domiciliary-failure/domiciliary-failure.module').then( m => m.DomiciliaryFailurePageModule)
  },
  {
    path: 'orden-pending-d',
    loadChildren: () => import('./pages/orden-pending-d/orden-pending-d.module').then( m => m.OrdenPendingDPageModule)
  },
  {
    path: 'tabs-admin',
    loadChildren: () => import('./pages/tabs-admin/tabs-admin.module').then( m => m.TabsAdminPageModule)
  },
  {
    path: 'lider',
    loadChildren: () => import('./pages/lider/lider.module').then( m => m.LiderPageModule)
  },
  {
    path: 'admin-lider',
    loadChildren: () => import('./pages/admin-lider/admin-lider.module').then( m => m.AdminLiderPageModule)
  },
  {
    path: 'register-lider',
    loadChildren: () => import('./pages/register-lider/register-lider.module').then( m => m.RegisterLiderPageModule)
  },
  
];

/*@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})*/

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true })
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],

  exports: [RouterModule]
})

export class AppRoutingModule { }
