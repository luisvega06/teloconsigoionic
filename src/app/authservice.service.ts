import { Injectable } from '@angular/core';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {

  access: boolean;
  token: string;

  baseURL = 'https://back.teloconsigo.net/api/';

  constructor() { }


  post(data, url, token){
    return  axios.post( data, url, {
      headers: {Authorization: token}
    });
  }


  get(url, token = null){
    return axios.get(url, {
        headers: { Authorization: token }
    });
  }
}
