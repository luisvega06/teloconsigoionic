import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthserviceService } from '../app/authservice.service';
import { JwtService } from '../app/jwt.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  user: any = {};
  userCopy: any = {};
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl:  NavController,
    private  authService :  AuthserviceService,
    private jwtService: JwtService,
    private router: Router,
    private menu: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this.getToken();
  }
  
  /*async getUser() {
    const token = localStorage.getItem('TokenTeloConsigo');
    if(token == null){

    }else{

    }
  }*/

  async closeMenu() {
    this.menu.close('first');
  }

  async sessionClose() {
    console.log("fin de session");
    localStorage.removeItem('TokenTeloConsigo');
    this.user = this.userCopy;
    this.menu.close('first');
    //this.getToken();
  }

  async action(id) {
    if(id == 0){
      this.menu.close('first');
      this.router.navigate(['/orders-management']);
      //this.navCtrl.navigateForward(['orders-management']);
    }
    if(id == 1){
      this.menu.close('first');
      this.router.navigate(['/confirmed-orders']);
      //this.navCtrl.navigateForward(['confirmed-orders']);
    }
    if(id == 2){
      this.menu.close('first');
      this.router.navigate(['/orders-delivered']);
      //this.navCtrl.navigateForward(['orders-delivered']);
    }
    if(id == 3){
      this.menu.close('first');
      this.router.navigate(['/undelivered-orders']);
      //this.navCtrl.navigateForward(['undelivered-orders']);
    }
    if(id == 4){
      this.menu.close('first');
      this.router.navigate(['/pending-orders']);
      //this.navCtrl.navigateForward(['pending-orders']);
    }
    if(id == 5){
      this.menu.close('first');
      this.router.navigate(['/domiciliary-delivered']);
      //this.navCtrl.navigateForward(['domiciliary-delivered']);
    }
    if(id == 6){
      this.menu.close('first');
      this.router.navigate(['/domiciliary-failure']);
      //this.navCtrl.navigateForward(['domiciliary-failure']);
    }
  }


  async getToken() {
    try{
		  const token = localStorage.getItem('TokenTeloConsigo');
		  if(token == null || token == undefined || token == ""){
        console.log("esta null o undefined");
			//this.navCtrl.navigateForward(['login']);
		  }else{
			console.log("funciona el menu1");
			this.user = this.jwtService.parseJwt(token);
			/*if(this.user.rol == 2){
			  //this.getOrdespending();
			}else{
			  this.navCtrl.navigateForward(['home-client']);
			}*/
		  }
		  
		  //console.log("el token vacio",this.user);
		}catch( ex){
		  console.log(ex);
		}
  }

  ionViewWillEnter() {
    this.getToken();
  }
}
